/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarreo;

/**
 *
 * @author Palto
 */
public class Persona {

    /* public enum SEXO{
     MASCULINO,
     FEMENINO
     }*/
    private String rut;
    private String username;
    private int edad;
    private String sexo;
    private boolean tieneExperiencia;
    private Computador computador;

    public Persona(String rut, String username, int edad, String sexo, boolean tieneExperiencia, Computador computador) {
        setRut(rut);
        setUsername(username);
        setEdad(edad);
        setSexo(sexo);
        setTieneExperiencia(tieneExperiencia);
        setComputador(computador);
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        if (edad >= 18 && edad <= 30) {
            this.edad = edad;
        } else {
            System.out.println("Edad debe ser entre 18 y 30 años");
        }
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        if (sexo.trim().toUpperCase().equals("F") || sexo.trim().toUpperCase().equals("M")) {
            this.sexo = sexo.toUpperCase();
        } else {
            System.out.println("Sexo debe ser F o M");
        }
    }

    public boolean isTieneExperiencia() {
        return tieneExperiencia;
    }

    public void setTieneExperiencia(boolean tieneExperiencia) {
        this.tieneExperiencia = tieneExperiencia;
    }

    public Computador getComputador() {
        return computador;
    }

    public void setComputador(Computador computador) {
        this.computador = computador;
    }
    
    public void imprimir(){        
        System.out.println("Rut: " + rut);
        System.out.println("Username: " + username);
        System.out.println("Edad: " + edad);
        System.out.println("Sexo: " + sexo);
        System.out.println("Tiene Experiencia: " + tieneExperiencia);
        computador.imprimir();        
    }

}
