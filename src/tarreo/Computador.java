/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tarreo;

/**
 *
 * @author Palto
 */
public class Computador {
    
    private String nombre;
    private String tarjeta;
    private int memoria;

    public Computador(String nombre, String tarjeta, int memoria) {
        this.nombre = nombre;
        this.tarjeta = tarjeta;
        this.memoria = memoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        if(tarjeta.trim().length()>= 2){
            this.tarjeta = tarjeta;
        }else{
            System.out.println("Tarjeta debe contener 2 caracteres mínimo");
        }
    }

    public int getMemoria() {
        return memoria;
    }

    public void setMemoria(int memoria) {
        if(memoria > 0){
            this.memoria = memoria;
        }else{
            System.out.println("Memoria de se mayor a 0");
        }
    }
    
    public void imprimir(){
        System.out.println("Nombre: " + nombre);
        System.out.println("Tarjeta: " + tarjeta);
        System.out.println("Memoria: " + memoria);        
    }
           
    
    
    
}
