/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarreo;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Palto
 */
public class Tarreo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
         Scanner sc = new Scanner(System.in);
         int capacidadMaxima;
         Duoc duoc;
         int opcion = 0;
         
         
         System.out.println("****** TARREO DUOC ******");
         System.out.println("Ingrese Cantidad de participantes del tarreo:");
         capacidadMaxima = Integer.parseInt(sc.nextLine());
         duoc = new Duoc(capacidadMaxima);
         
         do{
             System.out.println("Ingrese Opción");
             System.out.println("Opción 1: Agregar Personas");
             System.out.println("Opción 2: Listar Participantes");
             System.out.println("Opción 3: Ver cantidad novatos hombres");
             System.out.println("Opción 9: Salir");
             opcion = Integer.parseInt(sc.nextLine());
                          
             switch(opcion){
                 case 1:
                     
                     System.out.println("***** AGREGAR PERSONA *******");
                     System.out.print("Ingrese RUT: ");
                     String rut = sc.nextLine();
                     System.out.print("Ingrese Username: ");
                     String username = sc.nextLine();
                     int edad = 0;
                     do{
                     System.out.print("Ingrese Edad: ");
                     String edadAux = sc.nextLine();                     
                     try{
                         edad = Integer.parseInt(edadAux);
                     }catch(Exception e){
                         edad = 0;
                     }
                     }while(!(edad >=18 && edad <= 30));
                     
                     
                     
                     
                     System.out.print("Ingrese Sexo (F:FEMENINO/M:MASCULINO): ");
                     String sexo = sc.nextLine();
                     System.out.print("Ingrese Tiene experiencia (1:SI/2:NO): ");
                     int respuesta = Integer.parseInt(sc.nextLine());
                     boolean tieneExperiencia;
                     if(respuesta == 1){
                         tieneExperiencia = true;
                     }else{
                         tieneExperiencia = false;
                     }
                     //tieneExperiencia = (respuesta == 1)?true:false;
                     System.out.print("Ingrese Nombre computador: ");
                     String nombrePC = sc.nextLine();
                     
                     System.out.print("Ingrese Tarjeta computador: ");
                     String tarjetaPC = sc.nextLine();
                     
                     System.out.print("Ingrese Memoria RAM (Solo numeros): ");
                     int memoriaPC = Integer.parseInt(sc.nextLine());
                     
                     Persona persona = new Persona(rut, username, edad, sexo, tieneExperiencia, new Computador(nombrePC, tarjetaPC, memoriaPC));
                     
                     duoc.agregarPersona(persona);                    
                     
                     break;
                 case 2:
                     duoc.listarPersonas();
                     break;
                     
                 case 3:
                     //System.out.println("La cantidad de novatos hombres son: " + duoc.obtenerNovatosHombres());
                     for(Persona aux : duoc.obtienerNovatosHombres()){
                         aux.imprimir();
                     }
                     break;
             }
             
         }while(opcion != 9);
         System.out.println("****** CHAO ******");
         
    }
}
