/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tarreo;

import com.sun.xml.internal.ws.client.ContentNegotiation;

/**
 *
 * @author Palto
 */
public class Duoc {
    
    private Persona [] personas;
    private int cantidadParticipantes;
    private int capacidadMaxima;

    public Duoc(int capacidadMaxima) {
        this.capacidadMaxima = capacidadMaxima;
        personas = new Persona[capacidadMaxima];
        cantidadParticipantes = 0;
    }    
    
    public Persona[] getPersonas() {
        return personas;
    }

    public void setPersonas(Persona[] personas) {
        this.personas = personas;
    }

    public int getCantidadParticipantes() {
        return cantidadParticipantes;
    }

    public void setCantidadParticipantes(int cantidadParticipantes) {
        this.cantidadParticipantes = cantidadParticipantes;
    }

    public int getCapacidadMaxima() {
        return capacidadMaxima;
    }

    public void setCapacidadMaxima(int capacidadMaxima) {
        this.capacidadMaxima = capacidadMaxima;
    }
    
    public void agregarPersona(Persona persona){
        if(persona != null){
            if(cantidadParticipantes < capacidadMaxima){
                personas[cantidadParticipantes]=persona;
                cantidadParticipantes++;
            }else{
                System.out.println("No hay cupos");
            }
        }else{
            System.out.println("Persona no puede ser null");
        }
        System.out.println("cantidadParticipantes " + cantidadParticipantes);
    }
    
    public boolean isBuscarPersona(String rut){
        for(int x = 0; x < cantidadParticipantes; x++){
            if(personas[x].getRut().equals(rut)){
                return true;
            }
        }
        return false;
    }
    
    
    public void listarPersonas(){
        System.out.println("*********** LISTAR PERSONAS ****************");
        for(int x = 0; x < cantidadParticipantes; x++){
            System.out.println("Nombre Computador: " + personas[x].getComputador().getNombre());
            System.out.println("Nombre Usuario: " + personas[x].getUsername());
            System.out.println("Edad: " + personas[x].getEdad());
        }
    }
    
    /*public int obtenerNovatosHombres(){
        int cantidad = 0;
        for(int x = 0; x < cantidadParticipantes; x++){
            if(personas[x].getSexo().equals("M") && !personas[x].isTieneExperiencia()){
                //personas[x].imprimir();
                cantidad++;
            }
        }
        return cantidad;
    }*/
    
    public Persona[] obtienerNovatosHombres(){
        Persona[] personasNovatasHombres;
        int cantidad = 0;
        for(int x = 0; x < cantidadParticipantes; x++){
            if(personas[x].getSexo().equals("M") && !personas[x].isTieneExperiencia()){
                //personas[x].imprimir();
                cantidad++;
            }
        }
        personasNovatasHombres = new Persona[cantidad];
        cantidad = 0;
        for(int x = 0; x < cantidadParticipantes; x++){
            if(personas[x].getSexo().equals("M") && !personas[x].isTieneExperiencia()){
                //personas[x].imprimir();
                personasNovatasHombres[cantidad] = personas[x];
                cantidad++;
            }
        }
        return personasNovatasHombres;
    }
    
    
}
